<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->bigIncrements('resid');
            $table->biginteger('pickup_l')->unsigned();
            //$table->foreign('pickup_l')->references('lid')->on('locations');
            $table->biginteger('drop_l')->unsigned();
           // $table->foreign('drop_l')->references('lid')->on('locations');
            $table->datetime('pickup_start');
            $table->datetime('pickup_end');
            $table->biginteger('cuid')->unsigned();
            //$table->foreign('cuid')->references('cuid')->on('customers');
            $table->biginteger('cid')->unsigned();
            //$table->foreign('cid')->references('cid')->on('cars');
            $table->biginteger('did')->unsigned();
            //$table->foreign('did')->references('did')->on('drivers');
            $table->biginteger('pid')->unsigned();
            //$table->foreign('pid')->references('pid')->on('payments');
            $table->integer('luggage');
            $table->integer('passengers');
            $table->string('servicetype');
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
