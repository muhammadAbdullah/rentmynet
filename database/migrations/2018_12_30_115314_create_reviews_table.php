<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->bigIncrements('reid');
            $table->biginteger('cuid')->unsigned();
            //$table->foreign('cuid')->references('cuid')->on('customers');
            $table->biginteger('did')->unsigned();
            //$table->foreign('did')->references('did')->on('drivers');
            $table->string('comment')->nullable();
            $table->integer('stars');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
