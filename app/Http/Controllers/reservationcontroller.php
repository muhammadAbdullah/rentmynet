<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\reservation;
use App\Http\Controllers\carscontroller;
use App\Http\Controllers\customercontroller;
use App\Http\Controllers\drivercontroller;
use App\Http\Controllers\paymentcontroller;

class reservationcontroller extends Controller
{
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

     $cars = new carscontroller;
     $driver = new drivercontroller;
     $payment = new paymentcontroller;
     $allcar= $cars->showall();
     $alldriver = $driver->showall();
     $allpayment = $payment->showall();

      // echo "<pre>";
      //  print_r($allpayment);
      //  echo "<pre>";

       return view('add_reservation')->with('cars', $allcar)->with('drivers', $alldriver)->with('payments', $allpayment);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $visit=4;
       $data = $request->input();
      
          {
            reservation::insert(['date'=>$data['date'],'pickup_location'=>$data['pickup_l'], 'drop_location' => $data['drop_l'],  'pickup_t'=>$data['pickup_t'], 'drop_t'=>$data['drop_t'], 'cuid'=>$data['cuid'], 'did'=>$data['did'], 'pid'=>$data['pid'], 'cid'=>$data['cid'], 'luggage'=>$data['lagguage'], 'passengers'=>$data['passsengers']]);
            // $tbldata = DB::table('driver')->get();
            // print_r($tbldata);
          }
      
          return redirect()->back();
     }
     public function create_main(Request $request)
    {
        $customer= new customercontroller;
        $c_data=new customercontroller;
        $customerid= $customer->login_check($request);
          $cuid=0;
       foreach ($customerid as $cuid => $value ) {
         $cuid=$value->cuid;

       }

       $data = $request->input();

        $cuid=strval($cuid);

          {
            reservation::insert(['date'=>$data['date'],'pickup_location'=>$data['pickup_l'], 'drop_location' => $data['drop_l'],  'pickup_t'=>$data['pickup_t'], 'drop_t'=>$data['drop_t'], 'cuid'=>$cuid, 'did'=>$data['did'], 'pid'=>$data['pid'], 'cid'=>$data['cid'], 'luggage'=>$data['lagguage'], 'passengers'=>$data['passsengers']]);
          }
      
          return view('home');
     }
    public function show()
    {

       $reservation = reservation::get()->toArray();

       return view('list_reservation', ['reservations'=>$reservation]);
        //
    }
    public function showall()
    {

       $reservation = reservation::get()->toArray();

       return $reservation;
        //
    }
      
}
