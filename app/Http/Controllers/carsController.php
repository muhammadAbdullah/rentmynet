<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\car;

class carsController extends Controller
{
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       $data = $request->input();
       $tbldata = "";
       if (!empty($data))
       {


        $file = $_FILES['file'];
        $fileName = $_FILES['file']['name'];
        $fileTmpName = $_FILES['file']['tmp_name'];
        $fileSize = $_FILES['file']['size'];
        $fileError = $_FILES['file']['error'];
        $fileType = $_FILES['file']['type'];

        $fileEXT = explode('.',$fileName);

       $fileActualExt = strtolower(end($fileEXT));

       $allowed = array('jpeg','png','bmp','jif','jpg',);

       if (in_array($fileActualExt, $allowed)) {
        if ($fileError === 0) {
         if ($fileSize<50000000) {

          $fileDestination ='public\uploads/'.$fileName;
          move_uploaded_file($fileTmpName, $fileDestination);

          $filepath='public/uploads/'.$fileName;




      }
      else
      {
       echo "you cannot upload file of this size >50mb";
     }
      # code...
   }
   else
   {
    echo "you file has Error";
  }

}
else
{
  echo "you cannot upload file of this types!";
} 
          try
          {
            $newcar = new car();
            $newcar->name =$data['name'];
            $newcar->company =$data['company'];
            $newcar->air_condition =$data['air_condition'];
            $newcar->registeration =$data['registeration'];
            $newcar->no_of_passenger =$data['no_of_passenger'];
            $newcar->luggage =$data['luggage'];
            $newcar->img_destination=$filepath;
            $newcar->save();
             }
          catch(Exception $exp)
          {
            // $tbldata = DB::table('cars')->get();
            // print_r($tbldata);
            $request->session()->flash('alert-danger', $exp->getmessage());
            return redirect()->back();
          }
          $request->session()->flash('alert-success', 'User Added successfully!');
          return redirect()->back();
          
       }
       else
       {
           return redirect()->back();
       }
       
    }
    /**
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

       $cars = car::get()->toArray();

       return view('list_car')->with(['cars'=>$cars]);
        //
    }
    public function showall()
    {

       $cars = car::get()->toArray();

       return $cars;
        //
    }
    public function show_main()
    {

       $cars = car::get()->toArray();

       return view('fleet')->with(['cars'=>$cars]);
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editcar($id)
    {
      $cars = car::where('cid',$id)->first()->toArray();
      // echo "<pre>";
      // print_r($userdata);

      // echo $id;
      // die();
      return view('edit_car')->with('cars',$cars);
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatecar(Request $request)
    {
        $postdata=$request->input();
// 
        car::where('cid',$postdata['id'])->Update(['name'=>$postdata['name'],'company'=>$postdata['company'],'air_condition'=>$postdata['air_condition'],'registeration'=>$postdata['registeration'],'no_of_passenger'=>$postdata['no_of_passenger'],'luggage'=>$postdata['luggage']]);
        $cars = car::get()->toArray();
        return view('list_car')->with('cars',$cars);

        // echo "<pre>";
        // print_r($postdata);
        // die();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deletecar($id)
    {
        car::where('cid',$id)->delete();
        $cars = car::get()->toArray();
        return view('list_car')->with('cars',$cars);
    }
}
