<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\driver;
use App\location;

class drivercontroller extends Controller
{
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

     $all = driver::all();
   return view('list_driver', ['drivers' => $all]);
       // return;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       $data = $request->input();
        echo "<pre>";
       print_r($data);
       echo "<pre>";
       $tbldata = "";
       if (!empty($data))
       {
          try
          {
            $newdata = new driver();
            echo $newdata->name =$data['username'];
            echo $newdata->email =$data['Email'];
            echo $newdata->age =$data['age'];
            echo $newdata->phone =$data['phone'];
            echo $newdata->cnic =$data['cnic'];
            // $newdata->total_drive =$data['total_drive'];
           echo  $newdata->duty_start =$data['duty_start'];
           echo  $newdata->duty_end =$data['duty_end'];
           // $newlocation= new location();
           //   $newlocation->addresses =$data['addresses'];
           //   $newlocation->town =$data['town'];
           //   $newdlocation->city =$data['city'];
           //   $newdlocation->save();
           //   $locid=location::where('addresses',$data['addresses'])->first()->toArray();
           //   $newdata->locid=$locid;
             $newdata->save();
            //change text fields it miss match with database attributes

                  



           // driver::create(['name' => $data['name'], 'email'=>$data['email'], 'password'=>$data['pwd'] ]);
            // $tbldata = DB::table('drivers')->get();
            // print_r($tbldata);
          }
          catch(\Exception $exp)
          {
            // $tbldata = DB::table('drivers')->get();
            // print_r($tbldata);
            $request->session()->flash('alert-danger', $exp->getmessage());
            return redirect()->back();
          }
          $request->session()->flash('alert-success', 'driver Added successfully!');
          return redirect()->back();
          
       }
       else
       {
           return redirect()->back();
       }
       
    }
   public function createQuery(Request $request)
    {

     
       $data = $request->input();
      
          {
            driver::insert(['name' => $data['username'], 'email'=>$data['Email'], 'phone'=>$data['phone'],
            'cnic'=>$data['cnic'], 'age'=>$data['age'], 'duty_start'=>$data['duty_start'], 'duty_end'=>$data['duty_end'] ]);
            // $tbldata = DB::table('driver')->get();
            // print_r($tbldata);
          }
      
          return redirect()->back();
        
    }
    public function show()
    {

       $driver = driver::get()->toArray();

       return view('list_driver', ['drivers'=>$driver]);
        
    }
    public function showmain()
    {

       $driver = driver::get()->toArray();

       return view('ourdrivers', ['drivers'=>$driver]);
        
    }
    public function showall()
    {

       $driver = driver::get()->toArray();

       return $driver;
        
    }
    public function view($id)
    {

       $driver = driver::where('did',$id)->first()->toArray();

       return view('view_driver', ['driver'=>$driver]);
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editdriver($id)
    {
      $driver = driver::where('did',$id)->first()->toArray();
      // echo "<pre>";
      // print_r($driverdata);

      // echo $id;
      // die();
      return view('edit_driver')->with('driver',$driver);
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatedriver(Request $request)
    {
        $postdata=$request->input();

        driver::where('did',$postdata['driverid'])->Update(['name'=>$postdata['username'],'email'=>$postdata['Email'],'age'=>$postdata['age'],'phone'=>$postdata['phone'],'cnic'=>$postdata['cnic'],'duty_start'=>$postdata['duty_start'],'duty_end'=>$postdata['duty_end']]);
       
        $driver = driver::get()->toArray();
        return view('list_driver')->with('drivers',$driver);

        // echo "<pre>";
        // print_r($postdata);
        // die();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deletedriver($id)
    {
        driver::where('did',$id)->delete();
        $driver = driver::get()->toArray();
        return view('list_driver')->with('drivers',$driver);
    }

}
