<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
class driver extends Model
{
	protected $fillable = [ 
		'did','email','name','cnic','age','phone','total_drive','duty_start','duty_end'
	];

 }
