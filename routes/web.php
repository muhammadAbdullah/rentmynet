
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('about', function () {
// 	$user = array('Mustansar', 'CEO',10000,'MZSoftech');
//     return $user;
// });
// Route::get('profile', function () {
// 	$user = array('Mustansar', 'CEO',10000,'MZSoftech');
//     return $user;
// });
Route::get('/', function () {

	//$name = "Hello Laravell";
    //return view('welcome')->with("name",$name);
    //return view('welcome')->with(compact('name'));
    //return view('welcome', compact('name'));
    return view('home');
});

Route::post('/login_check', [
    'uses'=> 'adminsController@login'
]);
// Route::get('/admin', function () {
// 	return view('admin');
// });
Route::get('/admin/view', [
    'uses'=> 'adminsController@admin_view'
]);
Route::get('/login', function () {
	return view('login');
});
Route::get('/signup', function () {
    return view('signup');
});
Route::get('admin/admin_login', function () {
    return view('admin_login');
});
Route::post('admin/login_check', [
    'uses'=> 'adminsController@login'
]);
Route::get('admin/add_customer', function () {
	return view('add_customer');
});
Route::post('/create_customer', [
    'uses'=> 'customerController@createQuery'
]);
Route::get('admin/create_location', [
    'uses'=> 'locationController@create'
]);
Route::get('admin/list_customer', [
    'uses'=> 'customerController@show'
]);
Route::get('admin/view_customer{did}', [
    'uses'=> 'customerController@view'
]);
Route::get('admin/view_customer', function () {
    return view('view_customer');
});
Route::get('admin/edit_customer{id}', [
    'uses'=> 'customerController@editcustomer'
]);
Route::get('admin/delete_customer{id}', [
    'uses'=> 'customerController@deletecustomer'
]);
Route::post('admin/update_customer', [
    'uses'=> 'customerController@updatecustomer'
]);
Route::get('admin/add_car', function () {
	return view('add_car');
});
Route::post('admin/create_car', [
    'uses'=> 'carsController@create'
]);
Route::get('admin/list_car', [
    'uses'=> 'carsController@show'
]);
Route::get('admin/edit_car{cid}', [
    'uses'=> 'carsController@editcar'
]);
Route::post('admin/update_car', [
    'uses'=> 'carsController@updatecar'
]);
Route::get('admin/delete_car{id}', [
    'uses'=> 'carsController@deletecar'
]);
Route::get('admin/add_driver', function () {
	return view('add_driver');
});
Route::post('admin/create_driver', [
    'uses'=> 'driverController@createQuery'
]);
// Route::get('admin/view_driver', function () {
//     return view('view_driver');
// });
Route::get('admin/list_driver', [
    'uses'=> 'driverController@show'
]);
Route::get('admin/view_driver{did}', [
    'uses'=> 'driverController@view'
]);
Route::get('admin/edit_driver{id}', [
    'uses'=> 'driverController@editdriver'
]);
Route::get('admin/delete_driver{id}', [
    'uses'=> 'driverController@deletedriver'
]);
Route::post('admin/update_driver', [
    'uses'=> 'driverController@updatedriver'
]);
// Route::get('admin/add_reservation', function () {
//     return view('add_reservation');
// });
Route::get('admin/add_reservation', [
    'uses'=> 'reservationController@index'
]);
Route::post('admin/create_reservation', [
    'uses'=> 'reservationController@create'
]);
Route::post('/create_reservation_main', [
    'uses'=> 'reservationController@create_main'
]);
// Route::get('admin/list_reservation', function () {
//     return view('list_reservation');
// });
Route::get('admin/list_reservation', [
    'uses'=> 'reservationController@show'
]);
Route::get('/rates', function () {
	return view('rates');
});

Route::get('/contacts',function(){
    return view('contacts');
});
Route::get('/details',function(){
    return view('details');
});
// Route::get('/fleet',function(){
//     return view('fleet');
// });
Route::get('/fleet', [
    'uses'=> 'carsController@show_main'
]);
Route::get('/services',function(){
    return view('services');
});
Route::get('/about',function(){
    return view('about');
});
Route::get('/drivers', [
    'uses'=> 'driverController@showmain'
]);
// Route::get('/drivers',function(){
//     return view('ourdrivers');
// });

Route::get('/listusers','homecontroller@ListUser');
Route::post('/createuser','homecontroller@create');