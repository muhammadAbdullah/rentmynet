<!doctype html>
<html lang="{{ app()->getLocale() }}">
@include('layout.add_head')
@include('layout.add_header')

            <!-- Main content -->
               <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">List</h3>


            <div class="row">

              @foreach ($customers as $customer)
              <div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-blue">
              <div class="widget-user-image">
                <img class="img-circle" src="../public/dist/img/user8-128x128.jpg" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">{{$customer['name']}}</h3>
              <h5 class="widget-user-desc">visits{{$customer['visit']}}</h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a href="#">Email <span class="pull-right badge bg-aqua">{{$customer['email']}}</span></a></li>
                <li><a href="#">phone <span class="pull-right badge bg-green">{{$customer['c_phone']}}</span></a></li>
                <li><a href="#">Addresses <span class="pull-right badge bg-red">{{$customer['Addresses']}}</span></a></li>
                <li> 
                  <div class="box-tools pull-right">
                   <a href="{{ url('admin/view_customer'.$customer['cuid'])}}">  view<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button></a>
                   <a href="{{ url('admin/edit_customer'.$customer['cuid'])}}"> update<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button></a>
                   <a href="{{ url('admin/delete_customer'.$customer['cuid'])}}"> delete<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button></a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        </div>
        @endforeach
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


   @include('layout.add_footer')