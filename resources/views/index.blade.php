<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\admin ;
use App\Http\Controllers\carscontroller;
use App\Http\Controllers\customercontroller;
use App\Http\Controllers\drivercontroller;
use App\Http\Controllers\paymentcontroller;
use App\Http\Controllers\reservationcontroller;



     $cars = new carscontroller;
     $driver = new drivercontroller;
     $payment = new paymentcontroller;
     $customer = new customercontroller;
     $reservation = new reservationcontroller;
     $allcar= $cars->showall();
     $alldriver = $driver->showall();
     $allpayment = $payment->showall();
     $allcustomer = $customer->showall();
     $allreservation = $reservation->showall();

?>


<!doctype html>
<html lang="{{ app()->getLocale() }}">
@include('layout.head')
<body>
@include('layout.header')
@yield('content')
	<!-- // Content  -->
	<!-- Footer -->
	@include('layout.footer')
	<div class="remodal" data-remodal-id="modal">
		<!-- // order-details-form  -->
		<form class="order-details-form"  action="create_reservation_main" method="post" novalidate>
			<div class="inner-form">
				{{csrf_field()}}
				<h3>Order Form</h3>
				<div class="inner-form__elements">
					<div class="inner-half">
						<h5>New Reservation</h5>
						<div class="location">
							<input type="text" id="location1" name="pickup_l" placeholder="Your pickup location">
							<i class="icon-placeholder-for-map"></i>
						</div>
						<div class="location-drop-off">
							<input type="text" name="drop_l" placeholder="Enter drop-off location">
							<i class="icon-placeholder-for-map"></i>
						</div>
						<div class="inner-half__width">
							<div class="input-date datetimepicker-wrap">
								<input type="text" name="date" id="datetimepicker1" class="datetimepicker" placeholder="Pick-up date">
								<i class="icon-calendar-with-a-clock-time-tools"></i>
							</div>
							<div class="input-time">
								<input type="text" name="pickup_t" id="timepicker1" class="timepicker" placeholder="Pick-up time">
								<i class="icon-clock"></i>
							</div>
						</div>
						<div class="inner-half__width">
							<div class="input-date datetimepicker-wrap">
								<input type="text" name="date1" class="datetimepicker" placeholder="Drop-off date">
								<i class="icon-calendar-with-a-clock-time-tools"></i>
							</div>
							<div class="input-time">
								<input type="text" name="drop_t" class="timepicker" placeholder="Drop-off time">
								<i class="icon-clock"></i>
							</div>
						</div>

						<div class="select-vehicle">
							<select name="cid">
											<option value="Vehicle class">Vehicle </option>
											@foreach($allcar as $car)
											<option value="{{$car['cid']}}">{{$car['company']}}-{{$car['name']}}</option>
											@endforeach
										</select>
						</div>

						<div class="passengers-luggage">
							<div class="passengers">
								<span>Passengers*</span>
								<select name="passsengers">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
											</select>
							</div>
							<div class="luggage">
								<span>Luggage*</span>
								<select name="lagguage">
												<option value="10">10 kg</option>
												<option value="20">20 kg</option>
												<option value="30">30 kg</option>
												<option value="40">40 kg</option>
											</select>
							</div>
							<div class="carseat">
								<span></span>
								<div class="box-checkbox">
									<input type="checkbox" name="carseat" id="checkbox-03" value="yes">
									<label for="checkbox-03">Car Seat</label>
								</div>
							</div>
						</div>
					</div>
					<div class="inner-half">
						<h5>Passenger's Contact Info</h5>
						<div class="inner-half__width">
							<div class="your-phone">
								<input type="text" name="email" placeholder="Email" style="font: 20px">
							</div>
							<div class="email">
								<input type="text" name="password" placeholder="password">
							</div>
						</div>
						<div class="select-vehicle">
							<select name="did">
											<option value="Vehicle class">Driver </option>
											@foreach($alldriver as $driver)
											<option value="{{$driver['did']}}">{{$driver['name']}}</option>
											@endforeach
										</select>
						</div>
						<div class="payment">
							<span>Payment</span>
							<select name="pid">
											@foreach($allpayment as $payment)
											<option value="{{$payment['pid']}}">{{$payment['type']}}</option>
											@endforeach
										</select>
						</div>
					</div>
				</div>
				<input type="submit" class="btn" name="" value="submit">
			</div>
		</form>
		<!-- // order-details-form  -->
	</div>

	<!-- //Footer -->
	<!-- Google map -->
	<script src="public/js/jquery.1.12.4.min.js"></script>
	<script src="public/js/plugins/bootstrap.min.js"></script>
	<script src="public/js/plugins/wow.min.js"></script>
	<script src="public/js/plugins/jquery.smartmenus.min.js"></script>
	<script src="public/js/plugins/jquery.smartmenus.bootstrap.js"></script>
	<script src="public/js/plugins/jquery.nivo.slider.js"></script>
	<script src="public/js/plugins/swiper.min.js"></script>
	<script src="public/js/plugins/intlTelInput.min.js"></script>
	<script src="public/js/plugins/remodal.js"></script>
	<script src="public/js/plugins/stickup.min.js"></script>
	<script src="public/js/plugins/tool.js"></script>
	<script src="public/js/plugins/jquery.form.js"></script>
	<script src="public/js/plugins/jquery.validate.min.js"></script>
	<script src="public/js/plugins/moment.js"></script>
	<script src="public/js/plugins/bootstrap-datetimepicker.min.js"></script>
	<script src="public/js/custom.js"></script>
</body>


<!-- Mirrored from canada.tonytemplates.com/rent-a-car/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 16 Feb 2018 14:56:20 GMT -->
</html>