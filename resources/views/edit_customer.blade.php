<!doctype html>
<html lang="{{ app()->getLocale() }}">
@include('layout.add_head')
@include('layout.add_header')

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Customer</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                                <i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                                title="Remove">
                                <i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">





                        <form class="form-horizontal" role="form" action="../admin/update_customer" method="POST">
                            {{csrf_field()}}
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-12">
                     <input type="hidden" name="cuid" value="{{$customer['cuid']}}" >
                                        <h1> Upload the profile Picture</h1>
                                        <div class="avatar-wrapper">
                                            <img class="profile-pic" src="" />
                                            <div class="upload-button">
                                                <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
                                            </div>
                                            <input class="file-upload" type="file" accept="image/*" />
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Username</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="username" value="{{$customer['name']}}" placeholder="John Doe">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Password</label>
                                            <div class="col-sm-7">
                                                <input type="Password" class="form-control" name="password" value="{{$customer['password']}}" required
                                                >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Email</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="Email" value="{{$customer['email']}}" placeholder="JohnDoe@yahoo.com">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Phone Number</label>
                                            <div class="col-sm-10">
                                                <input type="phone" class="form-control" name="phone" value="{{$customer['c_phone']}}" placeholder="123456789">
                                            </div>
                                        </div>
                                    




                                        <h2>Address</h2>

                                      

                                        <div class="form-group">
                                            <p class="col-sm-offset-2 col-sm-10 help-block">Apartment, suite , unit,
                                                building, floor, etc.</p>
                                            <label for="inputAddressLine2" class="col-sm-2 control-label">Address</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="inputAddressLine2" name="addresses" value="{{$customer['Addresses']}}" placeholder="Address Line 2">
                                            </div>
                                        </div>

                            
                                        <input style="width: 200px;" type="submit" class="btn btn-block btn-primary btn-lg"
                                            value="submit" />
                        </form>
                    </div>
                </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer" align="right">


        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->

    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
 @include('layout.add_footer')