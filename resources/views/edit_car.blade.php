<!doctype html>
<html lang="{{ app()->getLocale() }}">
@include('layout.add_head')
@include('layout.add_header')
    <!-- Main content -->
    <section class="content">
     
      <!-- Default box -->
      <div class="box">
      <form action="../admin/update_car" method="POST">
     {{csrf_field()}}
        <div class="box-header with-border">
          <h3 class="box-title">Add Car</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <input type="hidden" name="id" value="{{$cars['cid']}}">
        <div class="box-body">
          <div class="form-group">
                  <label>Company Name</label>
                  <select class="form-control" required name="company" value="{{$cars['company']}}">
                    <option>Toyota</option>
                    <option>Honda</option>
                    <option>Suzuki</option>
                    <option>Hyundia</option>
                    <option>Mistubushi</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Model</label>
                  <select class="form-control" required name="model" value="">
                    <option value="2018">2018</option>
    <option value="2017">2017</option>
    <option value="2016">2016</option>
    <option value="2015">2015</option>
    <option value="2014">2014</option>
    <option value="2013">2013</option>
    <option value="2012">2012</option>
    <option value="2011">2011</option>
    <option value="2010">2010</option>
    <option value="2009">2009</option>
    <option value="2008">2008</option>
    <option value="2007">2007</option>
    <option value="2006">2006</option>
    <option value="2005">2005</option>
    <option value="2004">2004</option>
    <option value="2003">2003</option>
    <option value="2002">2002</option>
    <option value="2001">2001</option>
    <option value="2000">2000</option>
    <option value="1999">1999</option>
    <option value="1998">1998</option>
    <option value="1997">1997</option>
    <option value="1996">1996</option>
    <option value="1995">1995</option>
    <option value="1994">1994</option>
    <option value="1993">1993</option>
    <option value="1992">1992</option>
    <option value="1991">1991</option>
    <option value="1990">1990</option>
    <option value="1989">1989</option>
    <option value="1988">1988</option>
    <option value="1987">1987</option>
    <option value="1986">1986</option>
    <option value="1985">1985</option>
    <option value="1984">1984</option>
    <option value="1983">1983</option>
    <option value="1982">1982</option>
    <option value="1981">1981</option>
    <option value="1980">1980</option>
    <option value="1979">1979</option>
    <option value="1978">1978</option>
    <option value="1977">1977</option>
    <option value="1976">1976</option>
    <option value="1975">1975</option>
    <option value="1974">1974</option>
    <option value="1973">1973</option>
    <option value="1972">1972</option>
    <option value="1971">1971</option>
    <option value="1970">1970</option>
                  </select>
                </div>
                <label>Car Name</label>
                <input type="text" class="form-control"  placeholder="Enter Car Name" name="name" required value="{{$cars['name']}}">
                <div class="form-group">
                  <label>Pesgenger</label>
                  <select class="form-control" name="no_of_passenger">
                    <option>2</option>
                    <option>4</option>
                    <option>7</option>
                    <option>12</option>
                    <option>32</option>
                  </select>
                </div>
                <label>Registration Numer</label>
                <input type="text" class="form-control"  placeholder="LXG1234" name="registeration" required value="{{$cars['registeration']}}">
                <br>
                <div class="form-group">
                  <label>Lugage Cpacity</label>
                  <select class="form-control" name="luggage" value="{{$cars['luggage']}}">
                    <option>10 kg</option>
                    <option>20 kg</option>
                    <option>30 kg</option>
                    <option>50kg</option>
                    <option>100kg</option>
                  </select>
                </div>
             

                <div class="radio">
                    <label>
                      <input type="radio"  id="optionsRadios1" name="air_condition" value="yes" checked="">
                      Air Condition Available
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="air_condition" id="optionsRadios1" value="{{$cars['air_condition']}}" checked="">
                      Air Condition Not Available
                    </label>
                  </div>
                  <br><div class="container">
  <div class="row">
  <div class="col-sm-2 imgUp">
    <div class="imagePreview"></div>
<label class="btn btn-primary">
                              Upload<input type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
        </label>
  </div><!-- col-2 -->
  <i class="fa fa-plus imgAdd"></i>
 </div><!-- row -->
</div><!-- container -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer" align="right">
                <input style="width: 200px;"  class="btn btn-block btn-primary btn-lg" type="submit" value="submit">
        </div>
        <!-- /.box-footer-->
        </form>
      </div>
      <!-- /.box -->
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('layout.add_footer')