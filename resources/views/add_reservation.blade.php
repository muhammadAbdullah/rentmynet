<!doctype html>
<html lang="{{ app()->getLocale() }}">
@include('layout.add_head')
@include('layout.add_header')

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Reservation</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
            <div class="uk-section uk-section-default uk-padding-remove-horizontal">
      <div class="uk-container">
        <form class="uk-form-horizontal" id="application_form" action="../admin/create_reservation" method="post">
          <div class="Absolute-Center is-Responsive">
          <div class="uk-section-muted uk-padding">
            {{ csrf_field() }}

            <h2 class="uk-heading-divider">Time & Location</h2>
            <div class="uk-form-row">
            <div class="uk-margin-large-top">
            <div class="uk-margin-xlarge-left">
              <label for="form-h-it" class="uk-form-label">Pickup Date:</label>
              <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: location"></span>
                <input class="uk-input uk-form-width-large" id="name" type="text" name="date" required="required" aria-required="true" spellcheck="false" class="uk-form-large" placeholder="Enter the Pickup Date">
              </div>
            </div>
            </div>
          </div>

           <div class="uk-form-row">
            <div class="uk-margin-large-top">
            <div class="uk-margin-xlarge-left">
              <label for="form-h-it" class="uk-form-label">Pickup Location:</label>
              <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: location"></span>
                <input class="uk-input uk-form-width-large" id="name" type="text" name="pickup_l" required="required" aria-required="true" spellcheck="false" class="uk-form-large" placeholder="Enter the Pickup Location">
              </div>
            </div>
            </div>
          </div>

            
            <div class="uk-form-row">
            <div class="uk-margin-medium-top">
            <div class="uk-margin-xlarge-left">
              <label for="form-h-it" class="uk-form-label">Drop Location:</label>
              <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: location"></span>
                <input class="uk-input uk-form-width-large" id="name" type="text" name="drop_l" required="required" aria-required="true" spellcheck="false" placeholder="Enter the Drop Location">
              </div>
            </div>
            </div>
          </div>

         <div class="uk-form-row">
            <div class="uk-margin-medium-top">
            <div class="uk-margin-xlarge-left">
              <label for="form-h-it" class="uk-form-label">Pickup Time:</label>
              <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: clock"></span>
                <input type="time" id="name" class="uk-input uk-form-width-large"  required="required" aria-required="true" name="pickup_t" type="text" class="form-control timepicker" placeholder="Enter the PickUp Time" >
                
              </div>
            </div>
            </div>
          </div>

           <div class="uk-form-row">
            <div class="uk-margin-medium-top">
            <div class="uk-margin-xlarge-left">
              <label for="form-h-it" class="uk-form-label">Drop Time:</label>
              <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: clock"></span>
                <input type="time" id="name" class="uk-input uk-form-width-large"  required="required" aria-required="true" name="drop_t" type="text" class="form-control timepicker" placeholder="Enter the Drop Time" >
                
              </div>
            </div>
            </div>
          </div>


              
 
<br>
          <h2 class="uk-heading-divider">IDs of Reservers</h2>     
          <div class="uk-form-row">
            <div class="uk-margin-large-top">
            <div class="uk-margin-xlarge-left">
              <label for="form-h-it" class="uk-form-label">Customer ID:</label>
              <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: user"></span>
                <input class="uk-input uk-form-width-large" type="text" name="cuid" placeholder="Example.1234" required="required" aria-required="true" pattern=".*[0-9]$" spellcheck="false" title="Enter Correct Customer ID">
              </div>
            </div>
            </div>
          </div>


          <div class="uk-form-row">
            <div class="uk-margin-medium-top">
            <div class="uk-margin-xlarge-left">
              <label for="form-h-it" class="uk-form-label">Driver </label>
              <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: user"></span>
                <select class="uk-input uk-form-width-large" name="did">
                  @foreach ($drivers as $driver)
                  <option value="{{$driver['did']}}">
                    {{$driver['name']}}
                  </option>
                  @endforeach
                </select>
              </div>
            </div>
            </div>
          </div>


       
         
          <div class="uk-form-row">
            <div class="uk-margin-medium-top">
            <div class="uk-margin-xlarge-left">
              <label for="form-h-it" class="uk-form-label">Payment type:</label>
              <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: hashtag"></span>
               <select class="uk-input uk-form-width-large" name="pid">
                @foreach ($payments as $payment)
                  <option value="{{$payment['pid']}}">
                   {{$payment['type']}}
                  </option>
                  @endforeach
                </select>
              </div>
            </div>
            </div>
          </div>
    
<br>

  <div class="uk-form-row">
<h2 class="uk-heading-divider">Car Info</h2>


<div class="uk-form-row">
            <div class="uk-margin-large-top">
            <div class="uk-margin-xlarge-left">
              <label for="form-h-it" class="uk-form-label">Car ID:</label>
              <div class="uk-inline">
                <span class="uk-form-icon" uk-icon="icon: question"></span>
                <select class="uk-input uk-form-width-large" name="cid">
                  @foreach ($cars as $car)
                  <option value="{{$car['cid']}}">
                    {{$car['company']}}-{{$car['name']}}
                  </option>
                  @endforeach
                </select>
              </div>
            </div>
            </div>
          </div>

  
<div class="uk-form-row">
            <div class="uk-margin-medium-top" >
              <div class="uk-margin-xlarge-left">
              <label for="form-h-it" class="uk-form-label">Please Select The Laguage Cpacity?</label>
              <div uk-form-custom="target: > * > span:first">
                <select class="uk-select uk-form-width-large" id="class_main" name="lagguage" required>
                  <option value="">Select Luggage Cpacity</option>
                  <option value="50">50 kg</option>
                  <option value="60">60 kg</option>
                  <option value="70">70 kg</option>
                  <option value="80">90 kg</option>
                  <option value="90">90 kg</option>
                  <option value="100">100 kg</option>
                  <option value="120r">120 kg</option>
                  <option value="150">150 kg</option>
                  <option value="200">200 kg</option>
                </select>
                <button class="uk-button uk-button-default" type="button" tabindex="-1">
                  <span></span>
                  <span uk-icon="icon: chevron-down"></span>
                </button>
              </div>
            </div>
          </div>
                <br>
               
<div class="uk-form-row">
              <div class="uk-margin-medium-top">
              <div class="uk-margin-xlarge-left">
              <label for="form-h-it" class="uk-form-label">Please Select The Number of Passengers?</label>
              <div uk-form-custom="target: > * > span:first">
                <select class="uk-select uk-form-width-large" id="class_main" name="passsengers" required>
                  <option value="">Select Number of Passsengers</option>
                  <option value="1">only one Person</option>
                  <option value="2">Two Persons</option>
                  <option value="3">Three Persons</option>
                  <option value="4">Four Persons</option>
                  <option value="5">Five Persons</option>
                  <option value="8">Eight Persons(Hielux)</option>
                  <option value="16">sixteen Persons(Couch)</option>
                  <option value="28">Twenty Eirght Perosns(Bus)</option>
                  <option value="32">Thrtiy Two Persons(Bus)</option>
                </select>
                <button class="uk-button uk-button-default" type="button" tabindex="-1">
                  <span></span>
                  <span uk-icon="icon: chevron-down"></span>
                </button>
              </div>
            </div>
          </div>
        
                  
         <div class="uk-form-row">
            <div class="uk-margin-large-top">
            <div class="uk-margin-xlarge-left">
              
              <div class="uk-inline">
                <label for="form-h-it" class="uk-form-label"></label>
                <label for="form-h-it" class="uk-form-label"></label>
                <button class="uk-button uk-button-primary uk-button-large" type="submit">Save</button>
              </div>
            </div>
            </div>
          </div>
        </div>
        </form>
      </div>
    </div>
        </div>

      
    </div>
    

    <div class="box-footer" align="center">
        
        </div>

        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

   @include('layout.add_footer')

