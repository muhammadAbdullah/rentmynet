@extends('index')
@section('content')

	<!-- Content  -->
	<main id="page-content">
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="breadcrumbs__title">Our Drivers</div>
						<div class="breadcrumbs__items">
							<div class="breadcrumbs__wrap">
								<div class="breadcrumbs__item">
									<a href="index-2.html" class="breadcrumbs__item-link">Home</a> <span>/</span> <a href="ourdrivers.html" class="breadcrumbs__item-link">Our drivers</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- // Breadcrumbs  -->
		<section class="our-team-box">
			<div class="container">
				<div class="row">
					@foreach ($drivers as $driver)
					<div class="col-xs-6 col-md-4">
						<div class="block-team__item">
							<figure class="thumbnail">
								<img src="public/images/team_thumb-2.jpg" alt="">
							</figure>
							<div class="block-team__desc">
								<h3><a href="#">{{ $driver['name']}}</a></h3>
								<span class="position">Airport transfer driver</span>
								<p>Huge discounts. Free Delivery and Pickup. Free  Car washes. Free upgrades.</p>
							</div>
						</div>
					</div>
					@endforeach
					<div class="col-xs-12">
						<div class="pagination">
							<ul>
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
	<!-- // Content  -->
@stop